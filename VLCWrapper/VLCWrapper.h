#pragma once

#ifdef DLL_VLCWRAPPER_EXPORTS
#define DLL_VLCWRAPPER_API __declspec(dllexport)
#else
#define DLL_VLCWRAPPER_API __declspec(dllimport)
#endif

#include <vlc/vlc.h>
#include <windows.h>

class DLL_VLCWRAPPER_API CVLCWrapper
{
public:
    CVLCWrapper();
    ~CVLCWrapper();

public:
    int setup(const char* rmip, unsigned short port, int vb, int fps);
    int start();
    int stop();

    const char *VNAME = "screen_send";

private:
    libvlc_instance_t     *m_inst_send;

    char *m_rmip;
    unsigned short m_port;
    int m_vb;
    double m_fps;

    char *m_vlc_imrl;  //input MRL
    char *m_vlc_omrl;  //output MRL
    char *m_vlc_videv;  //video input device
    char *m_vlc_aidev;  //audio input device
    char *m_vlc_fps;
    char *m_vlc_aspect_ratio;
};

