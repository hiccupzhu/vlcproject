
// MFCPlayerDlg.h : header file
//

#pragma once
#include <VLCWrapper/SimplePlayer.h>

// CMFCPlayerDlg dialog
class CMFCPlayerDlg : public CDialogEx
{
// Construction
public:
	CMFCPlayerDlg(CWnd* pParent = NULL);	// standard constructor
    virtual ~CMFCPlayerDlg();

// Dialog Data
	enum { IDD = IDD_MFCPLAYER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnBnClickedBtnOpen();
    afx_msg void OnBnClickedBtnClose();

private:
    CSimplePlayer *mPlayer;
};
