#pragma once
#ifdef DLL_VLCWRAPPER_EXPORTS
#define DLL_VLCWRAPPER_API __declspec(dllexport)
#else
#define DLL_VLCWRAPPER_API __declspec(dllimport)
#endif


#include <windows.h>


struct libvlc_instance_t;
struct libvlc_media_player_t;

class DLL_VLCWRAPPER_API CSimplePlayer
{
public:
    CSimplePlayer(HWND hwnd = 0);
    ~CSimplePlayer();

    void SetHwnd(HWND hwnd);

    int OpenFile(const char* filename);

    int Play();
    int Stop();

private:
    libvlc_instance_t *vlcInstance;
    libvlc_media_player_t *vlcPlayer;

    HWND mHwnd;
};

