#ifndef __GDEF_H___
#define __GDEF_H___

#define DEBUG_PRINT (1)

#define PRINT_FPS() do{\
    static int fps = 0;\
    static int last_time = GetTickCount();\
    if (last_time + 1000 < GetTickCount()){\
        printf("  [%s] FPS is %d\n", __FUNCTION__, fps); \
        last_time = GetTickCount();\
        fps = 0;\
    }\
    else{\
        fps++;\
    }\
}while (0)


#ifndef SAFE_FREE
#define SAFE_FREE(x) \
do{\
if ((x) != NULL){\
    if (DEBUG_PRINT) printf("SAFE_FREE " #x "\n"); \
        free((x)); \
        (x) = NULL; \
}\
} while (0)
#endif

#ifndef SAFE_DELETE
#define SAFE_DELETE(x) \
do{\
if ((x) != NULL){\
    if (DEBUG_PRINT) printf("SAFE_FREE " #x "\n"); \
        delete (x); \
        (x) = NULL; \
}\
} while (0)
#endif

#ifndef SAFE_CLOSE
#define SAFE_CLOSE(x) \
do{\
    if ((x) != -1){\
        if (DEBUG_PRINT) printf("SAFE_CLOSE " #x "\n"); \
            close((x)); \
            (x) = -1; \
    }\
} while (0)
#endif

#ifndef SAFE_FCLOSE
#define SAFE_FCLOSE(x) \
do{\
        if (DEBUG_PRINT) printf("SAFE_FCLOSE " #x "\n"); \
        if ((x) != NULL){\
                fclose((x)); \
                (x) = NULL; \
        }\
} while (0)
#endif


#ifndef av_print
#define av_print(fmt, ...) \
do{\
printf("[%s::%s::%d]" fmt, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
} while (0)
#endif

#ifndef av_wprint
#define av_wprint(fmt, ...) \
do{\
printf("[%s::%s::%d]", __FILE__, __FUNCTION__, __LINE__); \
wprintf( fmt,  ##__VA_ARGS__); \
} while (0)
#endif

#endif