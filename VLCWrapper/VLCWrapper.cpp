#define DLL_VLCWRAPPER_EXPORTS

#include "VLCWrapper.h"
#include <utils/gdef.h>

#ifndef SAFE_VLM_RELEASE
#define SAFE_VLM_RELEASE(x) do{\
    if ((x) != NULL){\
        libvlc_vlm_release((x)); \
        (x) = NULL; \
    }\
}while (0)
#endif

CVLCWrapper::CVLCWrapper()
{
    m_inst_send = NULL;
    m_port = -1;
    m_rmip = NULL;
    m_vb = 0;
    m_fps = 0.0;

    m_vlc_imrl  = _strdup("dshow://");
    m_vlc_videv = _strdup("dshow-vdev=screen-capture-recorder");
    m_vlc_aidev = _strdup("dshow-adev=virtual-audio-capturer");
    m_vlc_omrl = NULL;
    m_vlc_fps = NULL;
    m_vlc_aspect_ratio = NULL;

}


CVLCWrapper::~CVLCWrapper()
{
    SAFE_FREE(m_rmip);
    SAFE_FREE(m_vlc_imrl);
    SAFE_FREE(m_vlc_omrl);
    SAFE_FREE(m_vlc_videv);
    SAFE_FREE(m_vlc_aidev);
    SAFE_FREE(m_vlc_fps);
    SAFE_FREE(m_vlc_aspect_ratio);
}

int CVLCWrapper::setup(const char* rmip, unsigned short port, int vb, int fps)
{
    SAFE_FREE(m_rmip);
    m_rmip = _strdup(rmip);
    m_port = port;
    m_vb = vb / 1000;
    m_fps = fps;

    SAFE_FREE(m_vlc_fps);
    SAFE_FREE(m_vlc_aspect_ratio);
    SAFE_FREE(m_vlc_omrl);

    const int BUF_SIZE = 0x4000;
    char* buffer = (char*)malloc(BUF_SIZE);
    

    sprintf_s(buffer, BUF_SIZE, "screen-fps=%.2f", m_fps);
    m_vlc_fps = _strdup(buffer);

    sprintf_s(buffer, BUF_SIZE, "dshow-aspect-ratio=%d:%d", 16, 9);
    m_vlc_aspect_ratio = _strdup(buffer);

    sprintf_s(buffer, BUF_SIZE,
        "#transcode{venc=x264{profile=high,keyint=25,min-keyint=5,ref=1,bframes=0,scenecut=40,\
         opengop=false,lookahead=3,level=41,pass=1,me=hex,merange=16,preset=medium,tune=zerolatency},\
         vcodec=h264,vb=%d,scale=1,acodec=mp3,ab=128,channels=2,samplerate=48000}:duplicate{\
         dst=standard{access=udp,mux=ts,dst=%s:%d}:\
         dst=standard{access=file,mux=ts, dst='scdump.ts'}\
         }",
         m_vb, m_rmip, m_port);
    m_vlc_omrl = _strdup(buffer);

    SAFE_FREE(buffer);

    return 0;
}


int CVLCWrapper::start()
{
    int ret = 0;
    if (m_inst_send != NULL){
        av_print("\n");
        return 0;
    }

    m_inst_send = libvlc_new(0, NULL);

    const char* params_send[5];
    
    params_send[0] = m_vlc_videv;
    params_send[1] = m_vlc_aidev;
    params_send[2] = m_vlc_fps;
    params_send[3] = m_vlc_aspect_ratio;
    params_send[4] = "live-caching=0";

    int nb_params = sizeof(params_send) / sizeof(params_send[0]);

    ret = libvlc_vlm_add_broadcast(m_inst_send,
        VNAME,
        m_vlc_imrl,
        m_vlc_omrl,
        nb_params, params_send, // <= 5 == sizeof(param) == count of parameters
        1, 0
    );

    if (ret != 0){
        av_print("    *** libvlc_vlm_add_broadcast() FAILED!!!\n");
        SAFE_VLM_RELEASE(m_inst_send);
        return ret;
    }

  
    ret = libvlc_vlm_play_media(m_inst_send, VNAME);
    if (ret != 0){
        av_print("    *** libvlc_vlm_play_media() FAILED!!!\n");
        SAFE_VLM_RELEASE(m_inst_send);
        return ret;
    }

    return 0;
}


int CVLCWrapper::stop()
{
    int ret = 0;
    ret = libvlc_vlm_stop_media(m_inst_send, VNAME);
    if (ret != 0){
        av_print("    *** libvlc_vlm_stop_media() FAILED!!!\n");
    }

    SAFE_VLM_RELEASE(m_inst_send);

    return 0;
}