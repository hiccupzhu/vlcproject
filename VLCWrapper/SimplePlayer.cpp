#include <vlc/vlc.h>

#define DLL_VLCWRAPPER_EXPORTS

#include "SimplePlayer.h"


CSimplePlayer::CSimplePlayer(HWND hwnd)
{
    vlcPlayer = NULL;

    /* Initialize libVLC */
    vlcInstance = libvlc_new(0, NULL);

    /* Complain in case of broken installation */
    if (vlcInstance == NULL) {
        MessageBoxW(NULL, L"Could not init libVLC", L"Qt libVLC player", MB_OK);
        exit(1);
    }

    SetHwnd(hwnd);
}

CSimplePlayer::~CSimplePlayer()
{
    Stop();

    if (vlcInstance){
        libvlc_release(vlcInstance);
        vlcInstance = NULL;
    }
}


void CSimplePlayer::SetHwnd(HWND hwnd)
{
    mHwnd = hwnd;
}


int CSimplePlayer::OpenFile(const char* filename)
{
    if (vlcPlayer && libvlc_media_player_is_playing(vlcPlayer))
        Stop();

    /* Create a new Media */
    libvlc_media_t *vlcMedia = libvlc_media_new_path(vlcInstance, filename);
    if (!vlcMedia)
        return -1;

    /* Create a new libvlc player */
    vlcPlayer = libvlc_media_player_new_from_media(vlcMedia);

    /* Release the media */
    libvlc_media_release(vlcMedia);

    /* Integrate the video in the interface */
    libvlc_media_player_set_hwnd(vlcPlayer, mHwnd);

    /* And start playback */
    libvlc_media_player_play(vlcPlayer);
    return 0;
}


int CSimplePlayer::Play()
{
    if (!vlcPlayer)
        return -1;

    if (libvlc_media_player_is_playing(vlcPlayer))
    {
        /* Pause */
        libvlc_media_player_pause(vlcPlayer);
    }
    else
    {
        /* Play again */
        libvlc_media_player_play(vlcPlayer);
    }

    return 0;
}


int CSimplePlayer::Stop()
{
    if (vlcPlayer) {
        /* stop the media player */
        libvlc_media_player_stop(vlcPlayer);

        /* release the media player */
        libvlc_media_player_release(vlcPlayer);

        /* Reset application values */
        vlcPlayer = NULL;
    }

    return 0;
}
